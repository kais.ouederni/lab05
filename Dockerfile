FROM python:latest  
COPY index.html /app/
WORKDIR /app
EXPOSE 8080
ENTRYPOINT ["python3", "-m", "http.server", "8080"]